import 'package:flutter/material.dart';
import 'package:flutter_contione/res/listData.dart';

class Custom_ListView_04 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(title: Text('FlutterDemo')),
      body: HomeContent(),
    );
  }
}

class HomeContent extends StatelessWidget {
  //自定义方法
  Widget _getListData(context, index) {
    return ListTile(
        title: Text(listData[index]["title"]),
        leading: Image.network(listData[index]["imageUrl"]),
        subtitle: Text(listData[index]["author"]));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView.builder(
        itemCount: listData.length, itemBuilder: this._getListData);
  }
}
