import 'package:flutter/material.dart';

class Custom_ListView_03 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(title: Text('FlutterDemo')),
      body: HomeContent(),
    );
  }
}

class HomeContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView(
      padding: EdgeInsets.all(10),
      children: <Widget>[
        Image.network(
            "http://www.contione.cn:8089/fileUpload/98036fd5-fb4b-4c81-a872-0a8c9bb9c65a.jpg"),
        Container(
          child: Text(
            '我是一个标题',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 28,
            ),
          ),
          height: 60,
          padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        ),
        Image.network(
            "http://www.contione.cn:8089/fileUpload/01f053e2-3f96-4e3d-99e7-75b90b8887c7.jpg"),
        Container(
          child: Text(
            '我是一个标题',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 28,
            ),
          ),
          height: 60,
          padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        ),
        Image.network(
            "http://www.contione.cn:8089/fileUpload/7a0976e3-42dd-4805-bd9e-3c1bce0a8b3b.jpg"),
        Container(
          child: Text(
            '我是一个标题',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 28,
            ),
          ),
          height: 60,
          padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        ),
        Image.network(
            "http://www.contione.cn:8089/fileUpload/ae4ab29b-b14d-4138-91f9-9ca624ad336a.png"),
        Container(
          child: Text(
            '我是一个标题',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 28,
            ),
          ),
          height: 60,
          padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        ),
        Image.network(
            "http://www.contione.cn:8089/fileUpload/4582ff3e-6f92-47a2-9f36-823731a10fbe.jpg"),
        Container(
          child: Text(
            '我是一个标题',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 28,
            ),
          ),
          height: 60,
          padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        ),
        Image.network(
            "http://www.contione.cn:8089/fileUpload/11d60745-84fc-4b46-bb1c-7ee1cbfa1cde.jpg"),
        Container(
          child: Text(
            '我是一个标题',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 28,
            ),
          ),
          height: 60,
          padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        ),
        Image.network(
            "http://www.contione.cn:8089/fileUpload/47e3705d-6cb0-4d1c-828f-f1b11d7c227a.jpeg"),
        Container(
          child: Text(
            '我是一个标题',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 28,
            ),
          ),
          height: 60,
          padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        ),
        Image.network(
            "http://www.contione.cn:8089/fileUpload/6a7d9df4-3447-4b53-addd-67468fcd9e05.jpg"),
        Image.network(
            "http://www.contione.cn:8089/fileUpload/ede0e234-11ea-4108-aef8-62a0ab2ce9ce.jpg"),
        Image.network(
            "http://www.contione.cn:8089/fileUpload/11d60745-84fc-4b46-bb1c-7ee1cbfa1cde.jpg"),
      ],
    );
  }
}
