import 'package:flutter/material.dart';

class Custopm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('AppBar'),
        ),
        body: HomeContent(),
      ),
      theme: ThemeData(primarySwatch: Colors.yellow), //主题颜色切换
    );
  }
}

//homecontent
class HomeContent extends StatelessWidget {
  const HomeContent({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Text(
          "你好自定义组件",
          textDirection: TextDirection.ltr,
          style: TextStyle(
            fontSize: 20.0,
            // color: Color.fromRGBO(244, 233, 121, 0.5)
            color: Colors.blue,
          ),
        ),
      ),
    );
  }
}
