import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          RaisedButton(
            child: Text("自定义组件"),
            onPressed: () {
              Navigator.pushNamed(context, '/custopm');
            },
          ),
          RaisedButton(
            child: Text("自定义组件Container"),
            onPressed: () {
              Navigator.pushNamed(context, '/container');
            },
          ),
          RaisedButton(
            child: Text("图片组件"),
            onPressed: () {
              Navigator.pushNamed(context, '/container_Image');
            },
          ),
          RaisedButton(
            child: Text("ListView"),
            onPressed: () {
              Navigator.pushNamed(context, '/container_ListView');
            },
          ),
          RaisedButton(
            child: Text("ListView_01"),
            onPressed: () {
              Navigator.pushNamed(context, '/container_ListView_01');
            },
          ),
          RaisedButton(
            child: Text("ListView_02"),
            onPressed: () {
              Navigator.pushNamed(context, '/container_ListView_02');
            },
          ),
          RaisedButton(
            child: Text("ListView_03"),
            onPressed: () {
              Navigator.pushNamed(context, '/container_ListView_03');
            },
          ),
          RaisedButton(
            child: Text("ListView_04"),
            onPressed: () {
              Navigator.pushNamed(context, '/container_ListView_04');
            },
          ),
          RaisedButton(
            child: Text("GridView"),
            onPressed: () {
              Navigator.pushNamed(context, '/container_GridView');
            },
          ),
          RaisedButton(
            child: Text("跳转到搜索"),
            onPressed: () {
              Navigator.pushNamed(context, '/search');
            },
          ),
          RaisedButton(
              child: Text("launcher"),
              onPressed: () {
                Navigator.pushNamed(context, '/launcher');
              }),
          RaisedButton(
              child: Text("弹性布局（Flex）"),
              onPressed: () {
                Navigator.pushNamed(context, '/flex');
              })
        ],
      ),
    );
  }
}
