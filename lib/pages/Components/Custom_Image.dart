import 'package:flutter/material.dart';

class Custom_Image extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(title: Text("flutter demo")), body: HomeContent());
  }
}

class HomeContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Container(
        width: 300.0,
        height: 300.0,
        decoration: BoxDecoration(
            color: Colors.yellow,
            borderRadius: BorderRadius.circular(150),
            image: DecorationImage(
                image: new NetworkImage(
                    'http://www.contione.cn:8089/fileUpload/98036fd5-fb4b-4c81-a872-0a8c9bb9c65a.jpg'),
                fit: BoxFit.cover)),
      ),
    );
  }
}

//本地图片
// return Center(
//         child: Container(
//       child: Image.asset(
//         'images/a.jpeg',
//         fit: BoxFit.cover,
//       ),
//       height: 300,
//       width: 300,

//     ));
//------------------------------------------------------------------------------------------------------------------
//远程图片
// return Center(
//       child: Container(
//           child: ClipOval(
//         child: Image.network(
//           "http://www.contione.cn:8089/fileUpload/98036fd5-fb4b-4c81-a872-0a8c9bb9c65a.jpg",
//           width: 150.0,
//           height: 150.0,
//         ),
//       )),
//     );
