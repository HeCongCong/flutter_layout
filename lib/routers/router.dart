import 'package:flutter/material.dart';
import '../pages/tabs/Tabs.dart';
import '../pages/Search.dart';
import '../pages/UrlLauncher.dart';
import '../pages/FlexWidget.dart';
import '../pages/Components/Custom.dart';
import '../pages/Components/Custom_Container.dart';
import '../pages/Components/Custom_Image.dart';
import '../pages/Components/Custom_ListView.dart';
import '../pages/Components/Custom_ListView_01.dart';
import '../pages/Components/Custom_ListView_02.dart';
import '../pages/Components/Custom_ListView_03.dart';
import '../pages/Components/Custom_ListView_04.dart';
import '../pages/Components/Custom_GridView.dart';

//配置路由
final routes = {
  '/': (context) => Tabs(),
  '/search': (context) => SearchPage(),
  '/launcher': (context) => UrlLauncher(),
  '/flex': (context) => FlexWidget(),
  '/custopm': (context) => Custopm(),
  '/container': (context) => Custom_Container(),
  '/container_Image': (context) => Custom_Image(),
  '/container_ListView': (context) => Custom_ListView(),
  '/container_ListView_01': (context) => Custom_ListView_01(),
  '/container_ListView_02': (context) => Custom_ListView_02(),
  '/container_ListView_03': (context) => Custom_ListView_03(),
  '/container_ListView_04': (context) => Custom_ListView_04(),
  '/container_GridView': (context) => Custom_GridView(),
};

//固定写法
var onGenerateRoute = (RouteSettings settings) {
// 统一处理
  final String name = settings.name;
  final Function pageContentBuilder = routes[name];
  if (pageContentBuilder != null) {
    if (settings.arguments != null) {
      final Route route = MaterialPageRoute(
          builder: (context) =>
              pageContentBuilder(context, arguments: settings.arguments));
      return route;
    } else {
      final Route route =
          MaterialPageRoute(builder: (context) => pageContentBuilder(context));
      return route;
    }
  }
};
